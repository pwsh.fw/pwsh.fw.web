# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.2.1] - 2023-10-04

### Fixed

- fixed function export list

## [1.2.0] - 2023-10-04

### Added

- `Trim-ULR` function

## [1.1.0] - 2021-09-21

### Added

-	:scroll: `Download-File` new parameter `-PassThru` to return fullName of downloaded file
-	:scroll: new function `Test-WebFileExist`

## [1.0.0] - 2021-02-17

-	:scroll: `ConvertTo-QueryString` -> convert complex objects to query strings
-	:scroll: `Download-File` -> download a file from the web
-	:scroll: `Download-FileWithRc` -> download a file from the web and return its http code
-	:scroll: `Get-UriWebRc` -> get the http code of an Uri
