write-enter "header.inc.pc1"

# load Pester helper file
. $PSScriptRoot/pester.inc.ps1

$ROOTDIR = (Resolve-Path $PSScriptRoot/../).Path
$TEMP = [System.IO.Path]::GetTempPath().TrimEnd('/\')
# Directory Separator char
$DS = [IO.Path]::DirectorySeparatorChar

# set logging
$Global:QUIET = $true
$Global:VERBOSE = $true
$Global:DEBUG = $true
$Global:DEVEL = $true

$pester = Import-Module Pester -MinimumVersion 4.0.0 -MaximumVersion 4.99.99 -ErrorAction Stop
if ((Get-Module Pester).Version -lt [version]'4.0.0') { throw "Pester > 4.0.0 is required." }
if ((Get-Module Pester).Version -ge [version]'5.0.0') { throw "Pester < 5.0.0 is required." }
$null = Import-Module PwSh.Fw.Core -DisableNameChecking

# get config
edevel "Get-Location = $(Get-Location)"
edevel "PSScriptRoot = $PSScriptRoot"
edevel "ROOTDIR = $ROOTDIR"
edevel "BASENAME = $BASENAME"
edevel "ModuleName = $ModuleName"

$VerbosePreference = 'continue'

# Mock Write-Error { } -ModuleName $ModuleName
# Mock Write-Host { }
# Mock Write-Host { } -ModuleName PwSh.Fw.Core
# Mock Write-Host { } -ModuleName PwSh.Fw.Write
# Mock Write-Host { } -ModuleName $ModuleName
# Mock New-Item { } -ModuleName $ModuleName

Write-Leave "header.inc.pc1"
