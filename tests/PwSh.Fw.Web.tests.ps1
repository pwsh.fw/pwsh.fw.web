# Invoke code coverage
# Invoke-Pester -Script ./PwSh.Fw.Web.tests.ps1 -CodeCoverage ../PwSh.Fw.Web/PwSh.Fw.Web.psm1

$BASENAME = Split-Path -Path $PSCommandPath -Leaf
$ModuleName = $BASENAME -replace ".tests.ps1"

# load header
. $PSScriptRoot/header.inc.ps1

Import-Module -FullyQualifiedName $ROOTDIR/$ModuleName/$ModuleName.psm1 -Force -ErrorAction stop

Describe "PwSh.Fw.Web" {

	Context "Return code handling" {

		It "Return 200 with an existing request" {
			$rc = Get-UriWebRc -url https://gitlab.com/pwsh.fw/pwsh.fw.web/-/raw/develop/LICENSE -AsInt
			$rc | Should Be 200
		}

		It "Return 'OK' with an existing request" {
			$rc = Get-UriWebRc -url https://gitlab.com/pwsh.fw/pwsh.fw.web/-/raw/develop/LICENSE -AsText
			$rc | Should Be "OK"
		}

		It "Return 404 with a non-existing request" {
			$rc = Get-UriWebRc -url https://gitlab.com/pwsh.fw/pwsh.fw.web/-/raw/develop/nonexistingfile.ps1 -AsInt
			$rc | Should Be 404
		}

		It "Return 'NotFound' with a non-existing request" {
			$rc = Get-UriWebRc -url https://gitlab.com/pwsh.fw/pwsh.fw.web/-/raw/develop/nonexistingfile.ps1 -AsText
			$rc | Should Be 'NotFound'
		}

	}

	Context "Downloading" {

        It "Return 200 with a successful download" {
            $rc = Download-FileWithRC -url https://gitlab.com/pwsh.fw/pwsh.fw.web/-/raw/develop/LICENSE -to TestDrive:\
            # $rc
            $rc | Should Be 200
            Test-Path -Path "TestDrive:\LICENSE" -PathType Leaf | Should Be $true
        }

        It "Return 404 with a non-successful download" {
            $rc = Download-FileWithRC -url https://gitlab.com/pwsh.fw/pwsh.fw.web/-/raw/develop/nonexistingfile.ps1 -to TestDrive:\
            # $rc
            $rc | Should Be 404
            Test-Path -Path "TestDrive:\nonexistingfile.ps1" -PathType Leaf | Should Be $false
        }

        It "Return 200 using WebClient" {
            $rc = Download-File -url https://gitlab.com/pwsh.fw/pwsh.fw.web/-/raw/develop/README.md -UseWebClient -to TestDrive:\
            # $rc
            $rc | Should Be 200
            Test-Path -Path "TestDrive:\README.md" -PathType Leaf | Should Be $true
        }

        It "Return 200 using WebRequest" {
            $rc = Download-File -url https://gitlab.com/pwsh.fw/pwsh.fw.web/-/raw/develop/VERSION -UseInvokeWebRequest -to TestDrive:\
            # $rc
            $rc | Should Be 200
            Test-Path -Path "TestDrive:\VERSION" -PathType Leaf | Should Be $true
        }

	}

	Context "Query string handling" {

		It "Return a query string from a one-dimensional array" {
			$q = ConvertTo-QueryString -InputObject @(1, 2, 4, 8, 1024, 2048) -variableName "array"
			$q | Should Be "array[0]=1&array[1]=2&array[2]=4&array[3]=8&array[4]=1024&array[5]=2048"
		}

		It "Return a query string from a two-dimensional array" {
			$q = ConvertTo-QueryString -InputObject @(@(1,1), @(2,1), @(4,2)) -variableName "array"
			$q | Should Be "array[0][0]=1&array[0][1]=1&array[1][0]=2&array[1][1]=1&array[2][0]=4&array[2][1]=2"
		}

		It "Return a query string from a simple hashtable" {
			$h = [ordered]@{"one" = 1; "two" = 2}
			$q = ConvertTo-QueryString -InputObject $h -variableName "hash"
			$q | Should Be "hash[one]=1&hash[two]=2"
		}

		It "Return a query string from an array of hashtable" {
			$q = ConvertTo-QueryString -InputObject @(@{"apple" = 1}, [ordered]@{"pear" = 2; "banana" = 4}) -variableName "array"
			$q | Should Be "array[0][apple]=1&array[1][pear]=2&array[1][banana]=4"
		}

	}

}

