[![Project Status: Active – The project has reached a stable, usable state and is being actively developed.](https://www.repostatus.org/badges/latest/active.svg)](https://www.repostatus.org/#active)
[![license](https://img.shields.io/github/license/cadegenn/pwsh.fw.web.svg)](LICENSE)

[![Pipeline status - master](https://gitlab.com/pwsh.fw/pwsh.fw.web/badges/master/pipeline.svg)](https://gitlab.com/pwsh.fw/pwsh.fw.web/pipelines)
[![Pipeline status - develop](https://gitlab.com/pwsh.fw/pwsh.fw.web/badges/develop/pipeline.svg)](https://gitlab.com/pwsh.fw/pwsh.fw.web/pipelines)

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/b95ad686a8d04676b39f7fd036d36083)](https://www.codacy.com/gl/pwsh.fw/pwsh.fw.web?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=pwsh.fw/pwsh.fw.web&amp;utm_campaign=Badge_Grade)

[![PowerShell Gallery - Version](https://img.shields.io/powershellgallery/v/Pwsh.fw.web)](https://www.powershellgallery.com/packages/Pwsh.fw.web)
[![PowerShell Gallery](https://img.shields.io/powershellgallery/dt/Pwsh.fw.web)](https://www.powershellgallery.com/packages/Pwsh.fw.web)
[![Powershell Platform](https://img.shields.io/powershellgallery/p/Pwsh.fw.web)](https://www.powershellgallery.com/packages/Pwsh.fw.web)

<img align="left" width="48" height="48" src="images/favicon.png">

# PwSh.Fw.Web

PwSh Framework Web module.

## Content

`Pwsh.fw.web` contains helper function to interact with web resources

## How it works

